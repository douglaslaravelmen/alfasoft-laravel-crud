@extends('layouts.main')

@section('content')
    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            Contacts List
        </div>
        <div class="card-body">
            @if (session()->has('error'))
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <i class="fa fa-times-circle"></i> {{ session('error') }}
                </div>
            @endif
            @if (session()->has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <i class="fa fa-check-circle"></i> {{ session('success') }}
                </div>
            @endif
            <table id="datatablesSimple">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Contact</th>
                        <th>Email Address</th>
                        <th>View</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Name</th>
                        <th>Contact</th>
                        <th>Email Address</th>
                        <th>View</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                </tfoot>
                <tbody>
                    @foreach ($contacts as $contact)
                        <tr>
                            <td>{{ $contact->name }}</td>
                            <td>{{ $contact->contact }}</td>
                            <td>{{ $contact->email_address }}</td>
                            <td><a href="{{ route('contact.show', ['contact' => $contact->id]) }}">View Contact
                                    Details</a></td>
                            <td><a href="{{ route('contact.edit', ['contact' => $contact->id]) }}">Edit Contact</a></td>
                            <td><a href="{{ route('contact.delete', ['contact' => $contact->id]) }}">Delete Contact</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
