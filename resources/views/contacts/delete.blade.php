@extends('layouts.main')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7">
                <div class="card shadow-lg border-0 rounded-lg mt-5">
                    <div class="card-header">
                        <h3 class="text-center font-weight-light my-4">Are you sure you want to delete the contact
                            #{{ $contact->id }}?</h3>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('contact.destroy', ['contact' => $contact->id]) }}">
                            @method('DELETE')
                            @csrf
                            <div class="row mb-3">
                                <div class="col-md-6">
                                    <div class="form-floating mb-3 mb-md-0">
                                        <input class="form-control" minlength="5" name="name" id="inputFirstName"
                                            type="text" placeholder="Enter the contact's name" readonly
                                            value="{{ $contact->name }}" />
                                        <label for="inputFirstName">Name</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-floating">
                                        <input class="form-control" maxlength="9" name="contact" id="inputLastName"
                                            type="text" placeholder="Enter the contact" readonly
                                            value="{{ $contact->contact }}" />
                                        <label for="inputLastName">Contact</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-floating mb-3">
                                <input class="form-control" id="inputEmail" name="email_address" type="email"
                                    placeholder="name@example.com" readonly value="{{ $contact->email_address }}" />
                                <label for="inputEmail">Email address</label>
                            </div>
                            <div class="mt-4 mb-0">
                                <div class="d-grid"><button type="submit" class="btn btn-primary btn-block">Yes,
                                        delete</button></div>
                            </div>
                            <div class="mt-4 mb-0">
                                <div class="d-grid"><a href="{{ route('contact.index') }}"
                                        class="btn btn-danger btn-block">No</a></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
