<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactCreationRequest;
use App\Http\Requests\ContactUpdateRequest;
use App\Repositories\ContactRepository;
use Illuminate\Support\Facades\Log;

class ContactController extends Controller
{
    protected $repository;

    public function __construct(ContactRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('contacts.index', ['contacts' => $this->repository->getAllContacts()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('contacts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContactCreationRequest $request)
    {
        try {
            $this->repository->storeContact($request->all());
            return redirect(route('contact.index'))->with('success', 'Contact registered successfully!');
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return redirect(route('contact.index'))->with('Error', 'Error registering the contact');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('contacts.show', ['contact' => $this->repository->getContactById($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('contacts.edit', ['contact' => $this->repository->getContactById($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ContactUpdateRequest $request, $id)
    {
        try {
            $this->repository->updateContact($request->all(), $id);
            return redirect(route('contact.edit', ['contact' => $id]))->with('success', 'Contact updated successfully!');
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return redirect(route('contact.edit', ['contact' => $id]))->with('Error', 'Error updating the contact');
        }
    }

    /**
     * Confirm the removal of a specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        return view('contacts.delete', ['contact' => $this->repository->getContactById($id)]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->repository->destroyContact($id);
            return redirect(route('contact.index'))->with('success', 'Contact deleted successfully!');
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return redirect(route('contact.index'))->with('Error', 'Error deleting the contact');
        }
    }
}
