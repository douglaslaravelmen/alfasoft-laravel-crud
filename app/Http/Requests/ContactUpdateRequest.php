<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ContactUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:5',
            'contact' => [
                'required',
                'size:9',
                Rule::unique('contacts', 'contact')->ignore($this->route('contact'))
            ],
            'email_address' => [
                'email',
                Rule::unique('contacts', 'email_address')->ignore($this->route('contact'))
            ]
        ];
    }
}
