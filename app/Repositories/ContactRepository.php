<?php

namespace App\Repositories;

use App\Models\Contact;

class ContactRepository
{

    public function getAllContacts()
    {
        return Contact::get(['id', 'name', 'contact', 'email_address']);
    }

    public function storeContact($data)
    {
        Contact::create($data);
    }

    public function getContactById($id)
    {
        return Contact::findOrFail($id);
    }

    public function updateContact($data, $id)
    {
        $contact = Contact::findOrFail($id);
        $contact->update($data);
    }

    public function destroyContact($id)
    {
        $contact = Contact::findOrFail($id);
        $contact->delete();
    }
}
