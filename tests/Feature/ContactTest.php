<?php

namespace Tests\Feature;

use App\Models\Contact;
use App\Models\User;
use Tests\TestCase;

class ContactTest extends TestCase
{
    public function test_required_name_validation()
    {
        $user = new User([
            'name' => 'John Doe',
            'email' => 'john.doe@email.com',
            'password' => bcrypt('123456789')
        ]);

        $this->be($user);

        $this->post('/contact', [
            'contact' => '000000000',
            'testing@email.com'
        ])->assertSessionHasErrors('name')->assertStatus(302);
    }

    public function test_min_name_validation()
    {
        $user = new User([
            'name' => 'John Doe',
            'email' => 'john.doe@email.com',
            'password' => bcrypt('123456789')
        ]);

        $this->be($user);

        $this->post('/contact', [
            'name' => 'Ivo',
            'contact' => '000000000',
            'testing@email.com'
        ])->assertSessionHasErrors('name')->assertStatus(302);
    }

    public function test_required_contact_validation()
    {
        $user = new User([
            'name' => 'John Doe',
            'email' => 'john.doe@email.com',
            'password' => bcrypt('123456789')
        ]);

        $this->be($user);

        $this->post('/contact', [
            'name' => 'Ivo',
            'testing@email.com'
        ])->assertSessionHasErrors('contact')->assertStatus(302);
    }

    public function test_max_contact_validation()
    {
        $user = new User([
            'name' => 'John Doe',
            'email' => 'john.doe@email.com',
            'password' => bcrypt('123456789')
        ]);

        $this->be($user);

        $this->post('/contact', [
            'name' => 'Ivo',
            'contact' => '1234567890',
            'testing@email.com'
        ])->assertSessionHasErrors('contact')->assertStatus(302);
    }

    public function test_unique_contact_validation()
    {
        $contactMock = Contact::create([
            'name' => 'Contact Mock',
            'contact' => '123465789',
            'email_address' => 'mock@email.com'
        ]);

        $user = new User([
            'name' => 'John Doe',
            'email' => 'john.doe@email.com',
            'password' => bcrypt('123456789')
        ]);

        $this->be($user);

        $this->post('/contact', [
            'name' => 'Ivo',
            'contact' => $contactMock->contact,
            'email_address' => 'testing@email.com'
        ])->assertSessionHasErrors('contact')->assertStatus(302);
    }

    public function test_email_email_address_validation()
    {
        $user = new User([
            'name' => 'John Doe',
            'email' => 'john.doe@email.com',
            'password' => bcrypt('123456789')
        ]);

        $this->be($user);

        $this->post('/contact', [
            'name' => 'Ivo',
            'contact' => '000000000',
            'email_address' => 'abcdef'
        ])->assertSessionHasErrors('email_address')->assertStatus(302);
    }

    public function test_unique_email_address_validation()
    {
        $contactMock = Contact::create([
            'name' => 'Mock Contact',
            'contact' => '987654321',
            'email_address' => 'email@mock.com'
        ]);

        $user = new User([
            'name' => 'John Doe',
            'email' => 'john.doe@email.com',
            'password' => bcrypt('123456789')
        ]);

        $this->be($user);

        $this->post('/contact', [
            'name' => 'Ivo',
            'contact' => '000000000',
            'email_address' => $contactMock->email_address
        ])->assertSessionHasErrors('email_address')->assertStatus(302);
    }
}
