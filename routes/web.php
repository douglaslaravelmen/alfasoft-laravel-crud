<?php

use App\Http\Controllers\ContactController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return redirect(route('contact.index'));
});

Route::group(['middleware' => 'auth'], function () {
    Route::resource('contact', ContactController::class, ['except' => ['index']]);
    Route::get('contact/delete/{contact}', [ContactController::class, 'delete'])->name('contact.delete');
});

Route::resource('contact', ContactController::class, ['only' => ['index']]);

require __DIR__.'/auth.php';
